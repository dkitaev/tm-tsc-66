package ru.tsc.kitaev.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.model.Task;

import java.util.List;

public interface ITaskService extends IService<Task> {

    @Nullable
    List<Task> findAllByProjectId(@Nullable final String projectId);

}
