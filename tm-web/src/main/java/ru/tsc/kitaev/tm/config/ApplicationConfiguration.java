package ru.tsc.kitaev.tm.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("ru.tsc.kitaev.tm")
public class ApplicationConfiguration {

}
