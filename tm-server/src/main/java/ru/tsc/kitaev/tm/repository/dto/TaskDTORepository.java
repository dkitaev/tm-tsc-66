package ru.tsc.kitaev.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Repository;
import ru.tsc.kitaev.tm.dto.TaskDTO;

import java.util.List;

@Repository
public interface TaskDTORepository extends AbstractOwnerDTORepository<TaskDTO> {

    @NotNull
    TaskDTO findByUserIdAndName(@NotNull String userId, @NotNull String name);

    void deleteByUserIdAndName(@NotNull String userId, @NotNull String name);

    @NotNull
    List<TaskDTO> findAllByUserIdAndProjectId(@NotNull String userId, @NotNull String projectId);

}
